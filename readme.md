Python version: `3.8.5`

You can test here: [https://demo-intetics.herokuapp.com/](https://demo-intetics.herokuapp.com/)

Notes for heroku:

* /admin/ is working (debug=True), credential is next:
    - username: admin;
    - password: admin;
* you can't donwload result files, because heroku don't allow to save them, so you can see output in paragraphs

Steps to run locally:

1. Create virtual environment `python -m venv env`
2. Activate _venv_
3. Install libs `pip install -r requirements.txt`
4. Create database in PostgreSQL
    - name: intetics
    - user: admin
    - password: admin
5. Make migrations `python manage.py makemigrations`
6. Migrate migrations `python manage.py migrate`
7. You can create super user if you need
    1. `python manage.py createsuperuser`
8. Finally run `python manage.py runserver`
