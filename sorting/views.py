
from django.conf import settings
from django.shortcuts import render
from django.views.generic import FormView
from .forms import SortingForm
from .models import Sorting
from .sort_algorithms import Bubble, Insertion, Merge


import datetime
import os
import sys


class Sort(FormView):
    template_name = 'sorting/index.html'
    form_class = SortingForm
    success_url = '/results/'


def str_to_class(classname):
    return getattr(sys.modules[__name__], classname)


def upload_txt(request):
    date = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    algorithm = str_to_class(request.POST['title'])
    txt_input = request.FILES['upload_file']
    txt_input_data = txt_input.read().decode('utf-8')
    file_input = '{}_{}'.format(date, txt_input.name)

    if not os.path.isdir(os.path.join(settings.BASE_DIR, 'result')):
        os.mkdir(os.path.join(settings.BASE_DIR, 'result'))

    with open(os.path.join(settings.MEDIA_ROOT, file_input), 'w') as f:
        # save downloaded file
        f.write(txt_input_data)

    B = algorithm(txt_input_data)
    B.read()

    log_time = {}
    B.sort(log_time=log_time)

    results = B.print_()

    file_output_name = f'result_{txt_input.field_name}_{date}.txt'

    with open(os.path.join(settings.MEDIA_ROOT, file_output_name), 'w') as f:
        f.writelines(', '.join([str(i) for i in results]))

    time_exec = log_time.get('sort', 0.0)

    r = Sorting(
        title=request.POST['title'],
        execution_time=time_exec,
        upload_file=file_input,
        sorted_file=file_output_name
    )
    r.save()

    return render(request, 'sorting/result.html', {
        'input_file_url': settings.MEDIA_URL + file_input,
        'input_file_name': txt_input.name,
        'input_list': [int(i) for i in txt_input_data.split(',')],
        'output_file_url': settings.MEDIA_URL + file_output_name,
        'output_file_name': f'result_{txt_input.name}',
        'output_list': results,
        'time_execution': time_exec,
        })
