from django.contrib import admin
from sorting.models import Sorting


@admin.register(Sorting)
class SoringAdmin(admin.ModelAdmin):
    date_hierarchy = 'date'
    search_fields = ['title', 'upload_file', 'sorted_file', 'date']
    list_display = ('title', 'execution_time', 'upload_file', 'sorted_file',
                    'date')
    list_filter = ('title',)
