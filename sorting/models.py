from django.db import models


ALGORITHMS_SORT = [
    ('Bubble', 'Bubble sorting'),
    ('Insertion', 'Insertion sorting'),
    ('Merge', 'Merge sorting'),
]


class Sorting(models.Model):
    title = models.CharField(max_length=100, choices=ALGORITHMS_SORT)
    execution_time = models.CharField(max_length=100, default=0.0)
    upload_file = models.FileField(default='')
    sorted_file = models.FileField(default='')
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return '{}_{}'.format(self.title, self.date)
