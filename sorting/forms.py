from .models import Sorting
from django.forms import ModelForm


class SortingForm(ModelForm):
    class Meta:
        model = Sorting
        fields = ['title', 'upload_file']
