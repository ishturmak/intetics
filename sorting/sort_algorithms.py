"""
    Script which include sorting algorithms. Here you
    can find Bubble, Insertion and Merge algorithms.
"""

from abc import ABC
import time


def time_execution(func):
    def wrapper(*args, **kwargs):
        time_start = time.time()
        func(*args, **kwargs)
        execution = time.time() - time_start

        if 'log_time' in kwargs:
            name = kwargs.get('log_name', func.__name__)
            kwargs['log_time'][name] = execution

    return wrapper


class Base(ABC):

    def __init__(self, array):
        self.array = array

    def read(self):
        self.raw_data = [int(i) for i in self.array.split(',')]

    def print_(self):
        return self.sorted

    def sort(self):
        pass


class Bubble(Base):

    def __init__(self, array):
        super(Bubble, self).__init__(array)

    @time_execution
    def sort(self, **kwargs):
        temp = self.raw_data
        for j in range(len(temp) - 1):
            for i in range(len(temp) - j - 1):
                if temp[i] >= temp[i+1]:
                    temp[i], temp[i+1] = temp[i+1], temp[i]

        self.sorted = temp


class Insertion(Base):

    def __init__(self, file):
        super(Insertion, self).__init__(file)

    @time_execution
    def sort(self, **kwargs):
        temp = self.raw_data
        for i in range(len(temp)):
            x = temp[i]
            j = i - 1
            while (j >= 0 and temp[j] > x):
                temp[j + 1] = temp[j]
                j -= 1
            temp[j + 1] = x

        self.sorted = temp


class Merge(Base):

    def __init__(self, file):
        super(Merge, self).__init__(file)

    @time_execution
    def sort(self, **kwargs):
        temp = self.raw_data
        self.sorted = Merge.mergesort(temp)

    def mergesort(m):
        if len(m) <= 1:
            return m
        else:
            middle = len(m) // 2
            left = Merge.mergesort(m[:middle])
            right = Merge.mergesort(m[middle:])
            result = Merge.merge(left, right)
            return result

    def merge(left, right):
        result = []
        while len(left) > 0 and len(right) > 0:
            if left[0] <= right[0]:
                result.append(left[0])
                left = left[1:]
            else:
                result.append(right[0])
                right = right[1:]
        while len(left) > 0:
            result.append(left[0])
            left = left[1:]
        while len(right) > 0:
            result.append(right[0])
            right = right[1:]
        return result


def main():
    A = Merge('test.txt')
    A.read()
    A.sort()
    A.print_()


if __name__ == "__main__":
    main()
