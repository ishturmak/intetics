from django.urls import path
from . import views

app_name = 'sorting'
urlpatterns = [
    path('', views.Sort.as_view(), name='main'),
    path('results', views.upload_txt, name='results'),
]
